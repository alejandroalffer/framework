import React, { ReactNode, useState } from "react";
import { Steps } from "intro.js-react";
import { steps } from "../tourSteps";

interface TourStepContextValue {
  setStepsEnabled: React.Dispatch<React.SetStateAction<boolean>>;
  isArtefactModalOpened: boolean;
}

export const TourStepContext = React.createContext<TourStepContextValue>({
  setStepsEnabled: () => {},
  isArtefactModalOpened: false,
});

export const TourStepProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [stepsEnabled, setStepsEnabled] = useState<boolean>(false);
  const [ref, setRef] = useState<Steps | null>(null);
  const [isArtefactModalOpened, setIsArtefactModalOpened] =
    useState<boolean>(false);

  const initialStep = 0;

  const refreshStepOnTransition = () => {
    if (ref) {
      setTimeout(() => {
        ref.introJs.refresh(true);
      }, 300);
    }
  };

  const handleStepChange = (nextStepIndex: number) => {
    const stepsWithModalOpened = [7, 8, 9];
    if (stepsWithModalOpened.includes(nextStepIndex)) {
      setIsArtefactModalOpened(true);
      /* refreshStepOnTransition();*/
    } else setIsArtefactModalOpened(false);
  };

  const handleExit = () => {
    setStepsEnabled(false);
  };
  return (
    <TourStepContext.Provider
      value={{
        setStepsEnabled,
        isArtefactModalOpened,
      }}
    >
      <>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={handleExit}
          onBeforeChange={handleStepChange}
          ref={(ref) => setRef(ref)}
        />
        {children}
      </>
    </TourStepContext.Provider>
  );
};
