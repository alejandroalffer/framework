export type ArtefactBgColors = { bg: string; hover: string };

export const calculateArtefactBgColor = (
  position: string,
  name: string
): ArtefactBgColors => {
  if (name === "Trust Framework") {
    return {
      bg: "linear(to-r, #AF00FA 30%, #110094 100%)",
      hover: "linear(to-r, #AF00FA 50%, #110094 100%)",
    };
  } else if (name === "Federated Catalogue") {
    return {
      bg: "grey",
      hover: "grey",
    };
  } else return calculateArtefactBgColorByRow(position);
};

function calculateArtefactBgColorByRow(
  artefactPosition: string
): ArtefactBgColors {
  const column = artefactPosition.split("/")[1].trim();

  switch (column) {
    case "2":
      return {
        bg: "linear(to-r, #AF00FA 70%, #6700ce 110%)",
        hover: 'linear(to-r, #AF00FA 60%, #110094 100%)"',
      };
    case "3":
      return {
        bg: "linear(to-r, #AF00FA 30%, #110094 100%)",
        hover: "linear(to-r, #AF00FA 50%, #110094 100%)",
      };
    case "4":
      return {
        bg: "primary",
        hover: 'linear(to-r, #AF00FA 10%, #110094 40%)"',
      };
    default:
      return {
        bg: "linear(to-r, #AF00FA 30%, #110094 100%)",
        hover: 'linear(to-r, #110094 90%, #AF00FA 10%)"',
      };
  }
}
