import { Header } from "./components/Header";
import {
  Flex,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
} from "@chakra-ui/react";
import { MainContainer } from "./components/MainContainer";
import React, { useContext, useState } from "react";
import { SoftwareContainer } from "./components/SoftwareContainer";
import { TourStepContext } from "./contexts/TourStepContext";
import { Footer } from "./components/Footer";
import { ClearingHouseContainer } from "./components/ClearingHouseContainer";

function App() {
  const [activeTab, setActiveTab] = useState<number>(0);

  const { setStepsEnabled } = useContext(TourStepContext);

  const handleStartTour = () => {
    setActiveTab(0);
    setStepsEnabled(true);
  };

  return (
    <div className="App">
      <Flex direction={"column"} p={8}>
        <Header startTour={handleStartTour} />
        <Tabs
          index={activeTab}
          onChange={setActiveTab}
          variant="enclosed"
          size={"lg"}
          className={"tabs"}
        >
          <TabList color={"primary"}>
            <Tab ml={"5%"}>Specifications</Tab>
            <Tab className={"software-tab"}>Software</Tab>
            <Tab>Clearing House</Tab>
          </TabList>

          <TabPanels>
            <TabPanel>
              <MainContainer setActiveTab={setActiveTab} />
            </TabPanel>
            <TabPanel>
              <SoftwareContainer />
            </TabPanel>
            <TabPanel>
              <ClearingHouseContainer />
            </TabPanel>
          </TabPanels>
        </Tabs>
        <Footer />
      </Flex>
    </div>
  );
}

export default App;
