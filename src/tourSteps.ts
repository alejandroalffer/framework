import { Step } from "intro.js-react";
import deliverablesImg from "./assets/deliverables.png";
import scopesImg from "./assets/scopes.png";

export const steps: Step[] = [
  // -------------- GENERIC --------------
  {
    title: "Hello 👋",
    intro:
      "Welcome in this quick tour of the <b>Gaia-X Framework</b> !<br>You can use the ⬅️ and ➡️ from your keyboard.<hr><b>Best experience on a FullHD screen in full screen (F11)</b>",
  },
  {
    element: ".tabs",
    title: "Gaia-X Framework",
    position: "top",
    intro: `This is a matrix representation of all the Gaia-X artefacts and how they relate to each other.`,
  },
  {
    element: ".Compliance",
    position: "right",
    title: "Gaia-X scopes 1/3",
    intro: `The <b>Gaia-X Compliance</b> for a common digital governance based on European values.<br><img src="${scopesImg}" width=250px">`,
  },
  {
    element: ".Federation",
    position: "left",
    title: "Gaia-X scopes 2/3",
    intro: `A mean for interoperability across <b>federated ecosystems</b>.<br><img src="${scopesImg}" width=250px">`,
  },
  {
    element: ".Data-Exchange",
    position: "top",
    title: "Gaia-X scopes 3/3",
    intro: `A mean to perform <b>data exchange</b> and anchor data contract negotiation results into the infrastructure.<br><img src="${scopesImg}" width=250px">`,
  },
  {
    element: ".specifications",
    title: "Gaia-X deliverables 1/2",
    position: "right",
    intro: `There are 2 types of <b>Gaia-X Specifications📄</b>:<ul>
        <li>Functional specifications</li>
        <li>Technical specifications</li>
        </ul><br><img src="${deliverablesImg}" width=250px">`,
  },
  {
    element: ".software-tab",
    position: "bottom",
    title: "Gaia-X deliverables 2/2",
    intro: `We have <b>open source implementation</b> for Services⚙️ and Tools📦 to operationalise Gaia-X.<br><img src="${deliverablesImg}" width=250px">`,
  },
  {
    element: ".releases",
    title: "Extra info per artefact 1/3",
    intro: `For each artefact, you have access to:
        <ul>
        <li>latest version</li>
        <li>next draft version</li>
        <li>previous version(s)</li>
        </ul>`,
  },
  {
    element: ".groups",
    title: "Extra info per artefact 2/3",
    intro: `The associated Gaia-X Working Groups open to all Gaia-X members.`,
  },
  {
    element: ".sections",
    title: "Extra info per artefact 3/3",
    intro: `The list of main sections in the document.`,
  },
  {
    title: "Thank you ! 💕",
    intro:
      'Your turn to browse this first knowledge base.<br>If you have any question, please reach out to <a href="mailto:info@gaia-x.eu">info@gaia-x.eu</a> or <a href="https://gitlab.com/gaia-x/technical-committee/framework" target="_blank">Gitlab</a>',
  },
];
