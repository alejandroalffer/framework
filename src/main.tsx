import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import "intro.js/introjs.css";
import "./index.css";
import { TourStepProvider } from "./contexts/TourStepContext";

const colors = {
  primary: "#110094",
  secondary: "#AF00FA",
};

const theme = extendTheme({ colors });

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <TourStepProvider>
        <App />
      </TourStepProvider>
    </ChakraProvider>
  </React.StrictMode>
);
