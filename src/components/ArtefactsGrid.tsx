import React, { Fragment } from "react";
import {
  Box,
  Center,
  Flex,
  Grid,
  GridItem,
  Heading,
  Text,
  useTheme,
} from "@chakra-ui/react";
import { descriptions, main } from "../data/main.json";
import { ArtefactType } from "../types";
import { Artefact } from "./Artefact";
import { calculateArtefactBgColor } from "../utils/calculateArtefactBgColor";

interface ArtefactsGridProps {
  setActiveTab: React.Dispatch<React.SetStateAction<number>>;
}

export const ArtefactsGrid: React.FC<ArtefactsGridProps> = ({
  setActiveTab,
}) => {
  const theme = useTheme();

  const deliverPlacements: Record<string, string> = {
    "Functional Specifications": "2 / 1 / 2 / 5",
    "Technical Specifications": "4 / 1 / 4 / 5",
  };

  const artefactPlacements: Record<string, string> = {
    "Policy-Rules-Label-Document": "2 / 2 / 3 / 3",
    "Architecture-Document": "2 / 3 / 3 / 5",
    "Trust-Framework": "4 / 2 / 5 / 6",
    "Identity-Access-Management": "6 / 3 / 7 / 4",
    "Federated-Catalogue": "5 / 3 / 6 / 4",
    "Gaia-X-Label": "3 / 1 / 4 / 2",
    "Data-Exchange-Services": "5 / 4 / 6 / 5",
  };

  const pillarsProps: Record<string, string> = {
    Compliance: "1 / 2 / 8 / 3",
    Federation: "1 / 3 / 8 / 4",
    "Data Exchange": "1 / 4 / 8 / 5",
  };

  const pillarsNameProps: Record<string, string> = {
    "Compliance-heading": "1 / 2 / 2 / 3",
    "Federation-heading": "1 / 3 / 2 / 4",
    "Data Exchange-heading": "1 / 4 / 2 / 5",
  };

  const hightlightsPlacements: Record<string, string> = {
    specifications: "2 / 1 / 6 / 2",
  };

  const pillars = Object.keys(pillarsProps);
  const hightlights = Object.entries(hightlightsPlacements);
  const mainArtefacts = Object.entries(main);

  return (
    <Box w={"100%"} pl={5} pt={5} className={"grid"} minW={"840px"}>
      <Grid
        templateColumns="repeat(4, 1fr)"
        templateRows="repeat(5, 10vh)"
        gap={4}
      >
        {pillars.map((pillar) => (
          <Fragment key={pillar}>
            <GridItem
              key={pillar}
              gridArea={pillarsProps[pillar]}
              mx={10}
              className={pillar === "Data Exchange" ? "Data-Exchange" : pillar}
            >
              <Flex
                boxSize={"100%"}
                pt={4}
                bgGradient={`linear(to-b, ${theme.colors.primary},rgb(158, 203, 230),rgb(255,255, 255))`}
                borderTopLeftRadius={"xl"}
                borderTopRightRadius={"xl"}
              ></Flex>
            </GridItem>
            <GridItem gridArea={pillarsNameProps[`${pillar}-heading`]}>
              <Center w={"100%"} h={"100%"}>
                <Heading
                  textAlign={"center"}
                  size={"lg"}
                  color={"white"}
                  zIndex={1}
                >
                  {pillar}
                </Heading>
              </Center>
            </GridItem>
          </Fragment>
        ))}
        {mainArtefacts.map(([deliver, artefacts], index) => (
          <Fragment key={deliver}>
            <GridItem gridArea={deliverPlacements[deliver]}>
              <Box
                borderTop={`5px solid ${theme.colors.secondary}`}
                py={3}
                position={"relative"}
              >
                <Heading size={"md"} color={"secondary"}>
                  {deliver}
                </Heading>
                <Text w={"20%"} mt={2} position={"absolute"} fontSize={12}>
                  {/* @ts-ignore */}
                  {descriptions[deliver]}
                </Text>
              </Box>
            </GridItem>
            {Object.entries(artefacts).map(([id, artefact], i) => (
              <GridItem
                key={id}
                gridArea={artefactPlacements[id]}
                zIndex={1}
                mt={6}
              >
                <Center>
                  <Artefact
                    index={i}
                    bgColors={calculateArtefactBgColor(
                      artefactPlacements[id],
                      (artefact as ArtefactType).name
                    )}
                    key={i}
                    artefact={artefact as ArtefactType}
                  />
                </Center>
              </GridItem>
            ))}
          </Fragment>
        ))}
        <GridItem
          className={"specifications"}
          gridArea={"2 / 1 / 6 / 2"}
        ></GridItem>
      </Grid>
    </Box>
  );
};
