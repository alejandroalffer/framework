import React from "react";
import {
  Button,
  Flex,
  Link,
  ListItem,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  Tooltip,
  UnorderedList,
  useDisclosure,
} from "@chakra-ui/react";
import { SoftwareArtefactType } from "../types";

interface SoftwareArtefactProps {
  artefact: SoftwareArtefactType;
  mandatoryIsChecked: boolean;
  version: string | undefined;
}

export const SoftwareArtefact: React.FC<SoftwareArtefactProps> = ({
  artefact,
  mandatoryIsChecked,
  version,
}) => {
  const { name } = artefact;

  const { isOpen, onOpen, onClose } = useDisclosure();

  const artefactsLinks = artefact.links[version!];

  const handleOpen = () => {
    if (version && artefactsLinks) {
      onOpen();
    }
  };

  return (
    <Tooltip
      label={"Select a version"}
      aria-label="Select a version tooltip"
      isDisabled={!!version}
      hasArrow
    >
      <Flex
        justifyContent={"center"}
        alignItems={"center"}
        bgGradient={
          artefactsLinks || !version
            ? "linear(to-r, #AF00FA 10%, #110094 55%)"
            : "none"
        }
        color={"white"}
        p={2}
        position={"relative"}
        borderRadius={"3px"}
        backgroundColor={
          artefactsLinks || !version ? "none" : "rgba(0,0,0,0.5)"
        }
        cursor={artefactsLinks ? "pointer" : "not-allowed"}
        onClick={handleOpen}
        border={
          mandatoryIsChecked && artefact.mandatory ? "2px solid red" : "none"
        }
        boxShadow={"base"}
        w={"100%"}
        h={"40px"}
      >
        <Text fontSize={"13"} fontWeight={"bold"}>
          {name}
        </Text>
        <Modal isOpen={isOpen} onClose={onClose} isCentered>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader color={"secondary"}>{name}</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              {artefactsLinks && (
                <UnorderedList p={4} color={"blue"}>
                  {artefactsLinks.document && (
                    <ListItem>
                      <Link href={artefactsLinks.document}>Document</Link>
                    </ListItem>
                  )}
                  {artefactsLinks.repository && (
                    <ListItem>
                      <Link href={artefactsLinks.repository}>Repository</Link>
                    </ListItem>
                  )}
                  {artefactsLinks.runningEndpoint && (
                    <ListItem>
                      <Link href={artefactsLinks.runningEndpoint}>
                        Running endpoint
                      </Link>
                    </ListItem>
                  )}
                </UnorderedList>
              )}
            </ModalBody>

            <ModalFooter>
              <Button
                bgGradient={"linear(to-r, #AF00FA 10%, #110094 55%)"}
                colorScheme={"facebook"}
                mr={3}
                onClick={onClose}
              >
                Close
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </Flex>
    </Tooltip>
  );
};
