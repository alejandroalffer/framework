import React from "react";
import { Flex, Heading, Link } from "@chakra-ui/react";
import { ExternalLinkIcon } from "@chakra-ui/icons";

export const ClearingHouseContainer: React.FC = () => {
  return (
    <Flex w={"100%"} direction={"column"} alignItems={"center"}>
      <Heading my={5}>Coming soon ...</Heading>
      <Link href={"https://gaia-x.eu/gxdch/"} isExternal>
        Learn more about Gaia-X Clearing House <ExternalLinkIcon mx="2px" />
      </Link>
    </Flex>
  );
};
