import React, { useState } from "react";
import { Flex } from "@chakra-ui/react";
import { SoftwareFilters } from "./SoftwareFilters";
import { SoftwareGrid } from "./SoftwareGrid";

export const SoftwareContainer: React.FC = () => {
  const [mandatoryIsChecked, setMandatoryIsChecked] =
    React.useState<boolean>(false);
  const [version, setVersion] = useState<string>("");

  return (
    <Flex>
      <SoftwareFilters
        setMandatoryIsChecked={setMandatoryIsChecked}
        setVersion={setVersion}
      />
      <SoftwareGrid mandatoryIsChecked={mandatoryIsChecked} version={version} />
    </Flex>
  );
};
