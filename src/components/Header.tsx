import React from "react";
import {
  Button,
  Flex,
  Heading,
  Image,
  ListItem,
  Text,
  UnorderedList,
} from "@chakra-ui/react";
import gxLogo from "../assets/gx-logo.jpg";

interface HeaderProps {
  startTour: () => void;
}

export const Header: React.FC<HeaderProps> = ({ startTour }) => {
  return (
    <Flex mb={6} justifyContent={"space-between"} color={"primary"}>
      <Flex direction={"column"}>
        <Flex alignItems={"center"}>
          <Image src={gxLogo} w={"100px"} h={"100px"} />
          <Heading>Gaia-X Framework</Heading>
        </Flex>
        <Flex
          direction={"column"}
          w={["100%", "50%"]}
          maxW={"1300px"}
          fontSize={12}
          color={"black"}
        >
          <Text>
            Gaia-X aims to connect the Data and Infrastructure Ecosystems and
            relies on 3 conceptual pillars to achieve that:
          </Text>
          <UnorderedList ml={10} mt={2}>
            <ListItem>
              Gaia-X Compliance: Decentralized services to enable objective and
              measurable trust
            </ListItem>
            <ListItem>
              Data Spaces / Federations: Interoperable & portable (Cross-)
              Sector data-sets and services
            </ListItem>
            <ListItem>
              Data Exchange: Anchored contract rules for access and data usage
            </ListItem>
          </UnorderedList>
          <Text mt={2}>
            In concrete terms, for each of these pillars there are 3 types of
            deliverables: Functional specifications, Technical Specifications
            and Software. This page offers a landscape of what is available and
            what are the planned releases. Click on the "Take the tour" button
            to explore the details of the landscape.
          </Text>
        </Flex>
      </Flex>

      <Flex alignItems={"center"} mt={4} minW={"300px"}>
        <Text>First time here ?</Text>
        <Button
          mx={4}
          colorScheme={"facebook"}
          bgGradient="linear(to-r, #AF00FA, #110094)"
          onClick={startTour}
        >
          Take the tour
        </Button>
      </Flex>
    </Flex>
  );
};
